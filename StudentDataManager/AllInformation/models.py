from __future__ import unicode_literals

from django.db import models

class StudentInformation(models.Model):
    roll_no=models.CharField(max_length=20,unique=True)
    first_name=models.CharField(max_length=30)
    middle_name=models.CharField(max_length=30,blank=True)
    last_name=models.CharField(max_length=30,blank=True)
    department=models.CharField(max_length=30)

    def __unicode__(self):

    	return str(self.roll_no)  +  ' ' +  str(self.first_name) + ' ' + str(self.middle_name) + ' ' + str(self.last_name) + ' ' + (self.department) + '\n'


class CourseEnroll(models.Model):
	course_id=models.CharField(max_length=10)
	course_name=models.CharField(max_length=20)
	course_instructur=models.CharField(max_length=20)
	student=models.ForeignKey(StudentInformation,on_delete=models.CASCADE)

	def __unicode__(self):
		return 	str(self.student.roll_no) + ' ' + str(self.course_id) + ' ' + str(self.course_name) + ' ' + str(self.course_instructur) + ' ' +  "\n"