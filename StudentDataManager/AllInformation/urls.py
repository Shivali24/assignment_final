from django.conf.urls import url
from AllInformation.views import StudentInformationView,CourseEnrollView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^all-info/', csrf_exempt(StudentInformationView.as_view())),
    url(r'^course-enroll/',csrf_exempt(CourseEnrollView.as_view()))
]