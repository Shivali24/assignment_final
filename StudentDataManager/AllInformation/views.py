from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse,QueryDict
from django.core.exceptions import ObjectDoesNotExist
import json
from AllInformation.models import StudentInformation,CourseEnroll

class StudentInformationView(View):
	def get(self,request,*args,**kwargs):  #to retrieve information from the server
		
		_param=request.GET
		
		try:
			query_set = StudentInformation.objects.get(roll_no=_param.get('roll_no'))
		except ObjectDoesNotExist:
			return HttpResponse("Record Does not Exist")
		query_set.delete()
		query_set=StudentInformation.objects.all()
		#query_set=StudentInformation.objects.filter(roll_no=_param.get('roll_no'))
		#query_set=StudentInformation.objects.excude(roll_no=_param.get('roll_no'))
		
		return HttpResponse((query_set), content_type='application/json')

	def post(self,request): # to store information to the server
		query_type=request.POST
		try:
			exist=StudentInformation.objects.get(roll_no=query_type.get('roll_no'))
		except ObjectDoesNotExist:
			info=StudentInformation(roll_no=query_type.get('roll_no'),first_name=query_type.get('first_name'),middle_name=query_type.get('middle_name'),last_name=query_type.get('last_name'),department=query_type.get('department'))
			info.save()
			return HttpResponse("Record inserted sucessfully!!")
		return HttpResponse("Unique Integrity error: Student Already exists")



	def put(self,request):
	 	data=QueryDict(request.body)
	 	try:
	 		exist_student=StudentInformation.objects.get(roll_no=data.get('roll_no'))
	 	except ObjectDoesNotExist:
	 		return HttpResponse("Student Record Does Not Exist")

	 	q=StudentInformation.objects.get(roll_no=data.get('roll_no'))
	 	q.roll_no=data.get('roll_no')
	 	q.first_name=data.get('first_name')
	 	q.middle_name=data.get('middle_name')
	 	q.last_name=data.get('last_name')
	 	q.department=data.get('department')
	 	q.save()
	 	return HttpResponse(" Record Successfully updated!!")


class CourseEnrollView(View):
	def post(self,request,*args,**kwargs):
		import pdb; pdb.set_trace()
		query_type=request.POST
		try:
			exist_student=StudentInformation.objects.get(roll_no=query_type.get('roll_no'))
		except:
			return HttpResponse("Foreign key Violated")
		CourseEnroll.objects.create(course_id=query_type.get('course_id'),course_name=query_type.get('course_name'),course_instructur=query_type.get('course_instructer'),student=exist)
		return HttpResponse("Record inserted successfullly!!")


	def get(self,request,*args,**kwargs):
		query_type=request.GET
		query_set=CourseEnroll.objects.all()
		return HttpResponse(query_set,content_type='application/json')

	def put(self,request):
		query_type=QueryDict(request.body)
		try:
			exist_student=StudentInformation.objects.get(roll_no=query_type.get('roll_no'))
		except:
			return HttpResponse("foreign key violated")
		try:
			exist_course=CourseEnroll.objects.get(course_id=query_type.get('course_id'))
		except:
			return HttpResponse("Entry does not exist")
		try:
			exist_course.course_id=query_type.get('new_course_id')
		except:
			return HttpResponse("unique integrity error!!")
		exist_course.course_name=query_type.get('course_name')
		exist_course.course_instructur=query_type.get('course_instructur')
		exist_course.save()
		return HttpResponse("Record Updated successfully")





